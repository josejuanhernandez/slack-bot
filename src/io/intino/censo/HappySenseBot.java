package io.intino.censo;

import com.ullink.slack.simpleslackapi.SlackChannel;
import io.intino.pandora.Bot;


public class HappySenseBot extends Bot {

    private static final String NotificationChannel = "tara";

    public HappySenseBot() {
        super("xoxb-13151025091-f3e0t6MeB461Jxt4SB0ySI9s");
        add("devices", "Show the list of connected devices", args -> showConnectedDevices());
        add("status", "Show the status of a device. Usage: status %deviceid", this::showDeviceStatus);
    }

    public void notify(String message) {
        send(NotificationChannel, message);
    }

    private String showConnectedDevices() {
        return "Connected devices";
    }

    private String showDeviceStatus(String[] args) {
        return "Status of " + args.length;
    }


}
