package io.intino.censo;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws Exception {
        HappySenseBot bot = new HappySenseBot();
        bot.execute();
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(() -> bot.notify("Son las " + new Date().toString() ), 10, 10, TimeUnit.SECONDS);
    }
}
